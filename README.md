# Kebab Dojo

*Disponible aussi en [Français](README_FR.md)*

---

## Context

This Dojo provides an implementation of a project in dotnet core following these principles:

- DDD - Domain Driven Design
- TDD - Test Driven Development
- CQS - Command Query Separation
- Hexagonal Architecture

It contains a theoretical presentation as well as a list of steps to practice on **DDD**, **TDD**, **CQS**, **Hexagonal architecture**.

The presentation aims to:

1. Help you get friendly with the concepts of this Dojo
2. Grant you support for organizing this DOJO

The DOJO is constituted of 13 successive steps.

## Prerequisites

This Dojo targets on C# and Java developers.

To be able to fulfill the steps you will need:

1. An IDE (preferably Visual Studio or Visual Studio Code)
2. Up to date dotnet Core install or gradle + Java
3. Git and a proper bash terminal (GitBash works)

## Workflow

### Presentation

Before diving in some code, please read or introduce the presentation. It contains crucial elements to understand the content and methodology of this DOJO. It also contains references to read more about our topics and go deeper into these concepts.

> **DISCLAIMER: Reading the presentation and practicing the DOJO will never be enough to fully understand the concepts presented here. Please keep on reading :)**

This presentation is in the PowerPoint file to ease organizing this DOJO.

### DOJO

This DOJO can be experienced as a puzzle game made of successive steps. For each step, the developer will have to implement code so that the tests pass.

Start by cloning the repository (you should land on master) and launch `next.sh` in your bash terminal.

For each time you think you have solved the step and want to move on to the next step please call again this `next.sh` script.

If you're stuck during a step and want to read again the instructions, please run `instruction.sh`

To start over run `reset.sh`  

For each step:

1. Please read the instructions (either given by `next.sh` or `instruction.sh`)
2. Execute the solution tests
3. Implement the corresponding code
4. run `next.sh` to validate your implementation and move on to the next step

Here are few rules:

- You never have to create new file during the development
- You don't need to write code outside of the method's body you are supposed to implement
- When moving on a new step branch, the code you produced is removed and replaced by our solution. If you want to keep it, I suggest you to commit it before running `next.sh`

### So what's next...

We built this DOJO because we believe in information sharing and the practices introduced here. We strongly encourage you to read, speak and share on these topics with your colleagues, friends, with us.

We don't know everything and this DOJO can surely be improved. If you have any thoughts or ideas about how to make it better, please feel free to submit an issue on our Gitlab project.

## DOJO Context and walk-through

A kebab shop wants to improve the customer experience by providing a brand new application to let them order their kebab. On this app, customers will be able to compose their kebab and send them to preparation.

You are a developer and you implement this application's backend.

### Step 1

#### Description

A customer can add ingredient to its kebab, you will implement this feature.

#### Technical

DDD: Implementation of an Aggregate / Entity composed with a ValueObject.
TDD:

- Use of NotImplementedException pattern
- Discovering the SUT pattern (System Under Test, cf [Mark Seemann](https://blog.ploeh.dk/) )
- Test method naming
- Use of comments Act / Arrange / Assert ([Roy Osherove](https://osherove.com/))

### Step 2

#### Description

Based on the kebab content, you have to determine whether the kebab is veggie or not.

### Step 3

#### Description

A kebab cannot contain more than 5 ingredients, you will implement this business rule.

### Step 4

#### Description

To order a kebab and send it to preparation, it must contains at least one ingredient, you will implement this verification.

### Step 5

#### Description

Once placed in preparation, the customer cannot modify its kebab anymore. You will disable kebab modification after the kebab's placement.

#### Technical

TDD: Use of the [pattern builder](https://blog.ploeh.dk/2017/08/15/test-data-builders-in-c/) to create complex objects in test cases.

### Step 6

#### Description

In this step, you will implement the raise of a domain event upon kebab placement.

#### Technical

DDD: Express domain modifications via domain events

### Step 7

#### Description

In Hexagonal Architecture the application layer is an adapter. This specific adapter contains the application use cases.

CQS Defines two types of interaction: command and queries.

In this step you will implement the "command" to create a kebab and place it for preparation.

#### Technical

DDD: Use of factory to instantiate aggregates

Hexagonal Architecture: Use of KebabFactory and Kebab API

CQS:

- Separate commands and queries at a code level
- Execute commands within application transaction scope

### Step 8

#### Description

Implement the query to retrieve kebab information.

#### Technical

DDD: Use of repository to load aggregate

### Step 9

#### Description

For adapters to listen to domain events, the domain provides a SPI "IDomainEventHandler". At the end of an application transaction all domain events must be executed by those handlers.

In ths step you will implement the validation of an application transaction as well as a domain event container.

### Step 10

#### Description

As for the application layer, the persistence is an adapter in our hexagonal architecture.
In this step you will implement the KebabRepository SPI in the persistence adapter.

#### Technical

TDD: Use of the InMemoryContext from EF

Hexagonal Architecture: SPI implementation

### Step 11

#### Description

Saving our kebab in the database is part of the persistence adapter. In this step you will implement the DomainEventHandler responsible for saving the kebab in the database.

#### Technical

Hexagonal architecture: Strict segregation between business and infrastructure.

### Step 12

#### Description

For a new adapter which wants to listen to a domain event that is already listened to, you just need to add a new domain event handler.

In this step you will implement a second handler responsible for sharing the event with the outside world.

### Step 13

Final step, no more code ! We implemented for you to more tests (of course they are failing) to show you how to test all the layer for a given use cases.
