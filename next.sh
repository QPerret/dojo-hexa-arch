#!/usr/bin/env bash

FILE=./.dojolang

if [ ! -f "$FILE" ]; then
    echo "$FILE does not exist."
    select lang in java dotnet
    do
        echo "Selected lang: $lang"
        echo ${lang} > ${FILE}
        break
    done
fi

if [ -f "$FILE" ]; then
    git checkout `cat .dojolang`-step-1 && ./instruction.sh
else
    echo "No lang selected."
    exit 1
fi